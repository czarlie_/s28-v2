// CRUD Operations
/*
  CRUD Operations are the heart of any backend application
*/

/*
Insert Documents (Create)
Inserting one document:
  Syntax:
    db.collectionName.insertOne({object})
    db.collectionName.insert({object})

  JavaScript syntax comparison
    object.object.method({object})
*/

db.users.insert({
  firstName: 'Jane',
  lastName: 'Doe',
  age: 21,
  contact: {
    phone: '87654321',
    email: 'janedoe@gmail.com',
  },
  courses: ['CSS', 'JavaScript', 'Python'],
  department: 'none',
})

/*
  Insert Many
    Syntax:
      db.collectionName.insertMany([{objectA}, {objectB}])
*/

db.users.insertMany([
  {
    firstName: 'Stephen',
    lastName: 'Hawking',
    age: 76,
    contact: {
      phone: '876-543-210',
      email: 'stephenhawking@mail.com',
    },
    courses: ['Python', 'React', 'PHP'],
    department: 'none',
  },
  {
    firstName: 'Neil',
    lastName: 'Armstrong',
    age: 82,
    contact: {
      phone: '876-210-543',
      email: 'neilarmstrong@mail.com',
    },
    courses: ['React', 'Laravel', 'Sass'],
    department: 'none',
  },
])

// Finding Documents (Read/Retrieve)
// Find
/*
  - If multiple documents match the criteria for finding a document, only the FIRST document that matches the search term will be returned
  - This is also based from the order that documents are stored in a collection
  - If the document is not found, the terminal will respond with a blank line

  Syntax:
  - db.collectionName.find()
  - db.collectionName.find({ field: value })
*/
// Finding a single document
// Leaving the search criteria empty will retrieve ALL documents
// Syntax:
db.users.find()
db.users.find({ firstName: 'Stephen' })

// Finding documents with multiple parameters
/*
  Syntax:
    db.collectionName.find({ fieldA: valueA, fieldB: valueB})
*/

// Mini-Activity #1
// Find a document with lastName: (Armstrong), age (82)
db.users.find({ lastName: 'Armstrong', age: 82 })

// Updating documents
// Update a single document
// create a document that will update:

db.users.insert({
  firstName: 'Test',
  lastName: 'Test',
  age: 0,
  contact: {
    phone: '000-000-000',
    email: 'test@mail.com',
  },
  courses: [],
  department: 'none',
})

/*
  Just like the .find(), methods that only manipulate a single document will only update the FIRST document that matches the search criteria

  Syntax:
    db.collectionName.updateOne({criteria}, {$set: { field: value }})
*/

db.users.updateOne(
  { firstName: 'Test' },
  {
    $set: {
      firstName: 'Bill',
      lastName: 'Gates',
      age: 65,
      contact: {
        phone: '123-456-789',
        email: 'billgates@mail.com',
      },
      courses: ['PHP', 'Lavarel', 'HTML'],
      department: 'Operations',
      status: 'active',
    },
  }
)

// Updating multiple documents
/*
  Syntax:
    db.collectionName.updateMany({criteria}, {$set: { field: value }})
*/

db.users.updateMany(
  { department: 'none' },
  {
    $set: { department: 'HR' },
  }
)

// replaceOne
/*
  Can be used if replacing the whole document is ncessary
  Syntax:
    db.collectionName.replaceOne({criteria}, {$set: { field: value }})
*/

db.users.replaceOne(
  { firstName: 'Bill' },
  {
    firstName: 'Bill',
    lastName: 'Gates',
    age: 65,
    contact: {
      phone: '123-456-789',
      email: 'bill@mail.com',
    },
    courses: ['PHP', 'Laravel', 'HTML'],
    department: 'Operations',
  }
)

// Deleting documents (delete)
// creating a document to delete

db.users.insert({
  firstName: 'test',
})

// delete a single document
/*
  db.collectionName.deleteOne({criteria})
*/

db.users.deleteOne({ firstName: 'test' })

// Delete many
/*
  Syntax:
    db.collectionName.deleteMany({criteria})
*/
db.users.deleteMany({ firstName: 'Bill' })

// Advanced Queries
// Query an embedded document

db.users.find({
  contact: {
    phone: '876-543-210',
    email: 'stephenhawking@mail.com',
  },
})

// query on nested field
db.users.find({ 'contact.email': 'janedoe@gmail.com' })

// querying an array with exact elements
db.users.find({ courses: ['CSS', 'Javascript', 'Python'] })

// querying an array without regard to order
db.users.find({ courses: { $all: ['React', 'Python'] } })

// quiry an embedded array
// add another document
db.users.insert({
  namearrr: [
    {
      namea: 'juan',
    },
    {
      nameb: 'tamad',
    },
  ],
})

db.users.find({
  namearrr: {
    namea: 'juan',
  },
})
